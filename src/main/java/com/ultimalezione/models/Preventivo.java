package com.ultimalezione.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "prodotti", "clienti"})
@Table
public class Preventivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_preventivo")
	private int id;
	@Column
	private String nome_azienda;
	@Column
	private String intestatario;
	@Column
	private String data_preventivo;
	@Column
	private String codice_preventivo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="indirizzo_azienda", referencedColumnName = "id_indirizzo", nullable = true)
	private Indirizzo indirizzo;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Prodotto_Preventivo",
			joinColumns = { @JoinColumn(name="preventivo", referencedColumnName = "id_preventivo") },
			inverseJoinColumns = { @JoinColumn(name="prodotto", referencedColumnName = "id_prodotto") })
	private List<Prodotto> prodotti;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Cliente_Preventivo",
			joinColumns = { @JoinColumn(name="preventivo", referencedColumnName = "id_preventivo") },
			inverseJoinColumns = { @JoinColumn(name="cliente", referencedColumnName = "id_cliente") })
	private List<Cliente> clienti;
	
	public Preventivo() {	
	}

	public Preventivo(String nome_azienda, String intestatario, String data_preventivo, String codice_preventivo) {
		this.nome_azienda = nome_azienda;
		this.intestatario = intestatario;
		this.data_preventivo = data_preventivo;
		this.codice_preventivo = codice_preventivo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}

	public String getIntestatario() {
		return intestatario;
	}

	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}

	public String getData_preventivo() {
		return data_preventivo;
	}

	public void setData_preventivo(String data_preventivo) {
		this.data_preventivo = data_preventivo;
	}
	

	public String getCodice_preventivo() {
		return codice_preventivo;
	}

	public void setCodice_preventivo(String codice_preventivo) {
		this.codice_preventivo = codice_preventivo;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}

	public List<Cliente> getClienti() {
		return clienti;
	}

	public void setClienti(List<Cliente> clienti) {
		this.clienti = clienti;
	}
	
}
