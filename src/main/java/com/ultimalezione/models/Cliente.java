package com.ultimalezione.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "preventivi"})
@Table
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_cliente")
	private int id;
	@Column
	private String nome_azienda;
	@Column
	private String intestatario;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="indirizzo", referencedColumnName = "id_indirizzo", nullable = true)
	private Indirizzo indirizzo;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Cliente_Preventivo",
			joinColumns = { @JoinColumn(name="cliente", referencedColumnName = "id_cliente") },
			inverseJoinColumns = { @JoinColumn(name="preventivo", referencedColumnName = "id_preventivo") })
	private List<Preventivo> preventivi;
	
	
	
	public Cliente () {
		
	}

	public Cliente(String nome_azienda, String intestatario) {
		this.nome_azienda = nome_azienda;
		this.intestatario = intestatario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}

	public String getIntestatario() {
		return intestatario;
	}

	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}

	public List<Preventivo> getPreventivi() {
		return preventivi;
	}

	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	
	

}
