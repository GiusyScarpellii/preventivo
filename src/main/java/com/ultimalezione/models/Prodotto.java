package com.ultimalezione.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "categorie", "preventivi"})
@Table
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_prodotto")
	private int id;
	@Column(name="codice_prodotto")
	private String codice;
	@Column(name="nome_prodotto")
	private String nome;
	@Column
	private int quantita;
	@Column
	private float prezzo;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Categoria_prodotto",
			joinColumns = { @JoinColumn(name="prodotto", referencedColumnName = "id_prodotto") },
			inverseJoinColumns = { @JoinColumn(name="categoria", referencedColumnName = "id_categoria") })
	private List<Categoria> categorie;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "Prodotto_Preventivo",
			joinColumns = { @JoinColumn(name="prodotto", referencedColumnName = "id_prodotto") },
			inverseJoinColumns = { @JoinColumn(name="preventivo", referencedColumnName = "id_preventivo") })
	private List<Preventivo> preventivi;
	
	public Prodotto() {
	}
	
	public Prodotto(String codice, String nome, int quantita, float prezzo) {
		this.codice = codice;
		this.nome = nome;
		this.quantita = quantita;
		this.prezzo = prezzo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public List<Categoria> getCategorie() {
		return categorie;
	}

	public void setCategorie(List<Categoria> categorie) {
		this.categorie = categorie;
	}

	public List<Preventivo> getPreventivi() {
		return preventivi;
	}

	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}
	

}
