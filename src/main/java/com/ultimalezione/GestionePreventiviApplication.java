package com.ultimalezione;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionePreventiviApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionePreventiviApplication.class, args);
	}

}
