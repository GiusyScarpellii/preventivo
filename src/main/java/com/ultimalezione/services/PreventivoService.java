package com.ultimalezione.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ultimalezione.models.Preventivo;
import com.ultimalezione.repository.DataAccessRepo;

@Service
public class PreventivoService implements DataAccessRepo<Preventivo>{

	
	@Autowired
	private EntityManager entMan;
	
	protected Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Preventivo insert(Preventivo t) {
		
		Preventivo temp = new Preventivo();
		temp.setNome_azienda(t.getNome_azienda());
		temp.setIntestatario(t.getIntestatario());
		temp.setData_preventivo(t.getData_preventivo());
		
		Session session = getSession();
		
		try {
			session.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Preventivo> findAll() {
		Session session = getSession();
		return session.createCriteria(Preventivo.class).list();
	}

	@Override
	public Preventivo findById(int varId) {
		Session session = getSession();
		return(Preventivo) session
							.createCriteria(Preventivo.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}

	@Override
	@Transactional
	public boolean delete(int varId) {
		
		Session session = getSession();
		try {
			Preventivo temp = session.load(Preventivo.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	@Transactional
	public boolean update(Preventivo t) {
		Session session = getSession();
		try {
			Preventivo temp = session.load(Preventivo.class, t.getId());
			if(temp != null) {
				temp.setNome_azienda(t.getNome_azienda());
				temp.setIntestatario(t.getIntestatario());
				temp.setData_preventivo(t.getData_preventivo());
				
				session.update(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
				System.out.println(e.getMessage());
		}
			
		return false;
	}
	
	

}
