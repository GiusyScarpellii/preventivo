package com.ultimalezione.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ultimalezione.models.Categoria;
import com.ultimalezione.repository.DataAccessRepo;

@Service
public class CategoriaService implements DataAccessRepo<Categoria>{
	
	@Autowired
	private EntityManager entMan;						
	
	private Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Categoria insert(Categoria t) {
		
		Categoria temp = new Categoria();
		temp.setNome(t.getNome());
		
		Session session = getSession();
		
		try {
			session.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		return temp;
	}

	@Override
	public List<Categoria> findAll() {
		
		Session session = getSession();
		return session.createCriteria(Categoria.class).list();
	}

	@Override
	public Categoria findById(int varId) {
		
		Session session = getSession();
		
		return (Categoria) session
					.createCriteria(Categoria.class)
					.add(Restrictions.eqOrIsNull("id", varId))
					.uniqueResult();
	}

	@Override
	@Transactional
	public boolean delete(int varId) {
		
		Session session = getSession();
		try {
			Categoria temp = session.load(Categoria.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	@Transactional
	public boolean update(Categoria t) {
		Session session = getSession();
		
		try {
			Categoria temp = session.load(Categoria.class, t.getId());
			if(temp != null) {
				temp.setNome(t.getNome());
				
				session.update(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

}
