package com.ultimalezione.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ultimalezione.models.Preventivo;
import com.ultimalezione.models.Prodotto;
import com.ultimalezione.repository.DataAccessRepo;

@Service
public class ProdottoService implements DataAccessRepo<Prodotto>{

	
	@Autowired
	private EntityManager entMan;
	
	protected Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Prodotto insert(Prodotto t) {
		
		Prodotto temp = new Prodotto();
		temp.setCodice(t.getCodice());
		temp.setNome(t.getNome());
		temp.setQuantita(t.getQuantita());
		temp.setPrezzo(t.getPrezzo());
		
		Session session = getSession();
		
		try {
			session.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Prodotto> findAll() {
		Session session = getSession();
		return session.createCriteria(Prodotto.class).list();
	}

	@Override
	public Prodotto findById(int varId) {
		Session session = getSession();
		return(Prodotto) session
							.createCriteria(Prodotto.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}

	@Override
	@Transactional
	public boolean delete(int varId) {
		
		Session session = getSession();
		try {
			Prodotto temp = session.load(Prodotto.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	@Transactional
	public boolean update(Prodotto t) {
		
		Session session = getSession();
		try {
			Prodotto temp = session.load(Prodotto.class, t.getId());
			if(temp != null) {
				temp.setCodice(t.getCodice());
				temp.setNome(t.getNome());
				temp.setQuantita(t.getQuantita());
				temp.setPrezzo(t.getPrezzo());
				
				session.update(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
	}
		
		return false;
	}

	

}
