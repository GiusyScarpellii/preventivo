package com.ultimalezione.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ultimalezione.models.Cliente;
import com.ultimalezione.repository.DataAccessRepo;

@Service
public class ClienteService implements DataAccessRepo<Cliente>{

	
	@Autowired
	private EntityManager entMan;
	
	protected Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	
	@Override
	public Cliente insert(Cliente t) {
		
		Cliente temp = new Cliente();
		temp.setNome_azienda(t.getNome_azienda());
		temp.setIntestatario(t.getIntestatario());
		
		Session session = getSession();
		
		try {
			session.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Cliente> findAll() {
		
		Session session = getSession();
		return session.createCriteria(Cliente.class).list();
	}

	@Override
	public Cliente findById(int varId) {
		
		Session session = getSession();
		return(Cliente) session
							.createCriteria(Cliente.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}

	@Override
	@Transactional
	public boolean delete(int varId) {
		
		Session session = getSession();
		try {
			Cliente temp = session.load(Cliente.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	@Transactional
	public boolean update(Cliente t) {
		
		Session session = getSession();
		try {
			Cliente temp = session.load(Cliente.class, t.getId());
			if(temp != null) {
				temp.setNome_azienda(t.getNome_azienda());
				temp.setIntestatario(t.getIntestatario());
				
				session.update(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
				System.out.println(e.getMessage());
		}
		return false;
	}


	
}
