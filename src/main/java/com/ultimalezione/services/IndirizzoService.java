package com.ultimalezione.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.ultimalezione.models.Indirizzo;
import com.ultimalezione.repository.DataAccessRepo;

@Service
public class IndirizzoService implements DataAccessRepo<Indirizzo>{

	
	@Autowired
	private EntityManager entMan;
	
	protected Session getSession() {
		return entMan.unwrap(Session.class);
	}
	
	@Override
	public Indirizzo insert(Indirizzo t) {
		
		Indirizzo temp = new Indirizzo();
		temp.setIndirizzo(t.getIndirizzo());
		temp.setCitta(t.getCitta());
		temp.setCap(t.getCap());
		temp.setProvincia(t.getProvincia());
		
		Session session = getSession();
		
		try {
			session.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Indirizzo> findAll() {
		Session session = getSession();
		return session.createCriteria(Indirizzo.class).list();
	}

	@Override
	public Indirizzo findById(int varId) {
		
		Session session = getSession();
		return(Indirizzo) session
							.createCriteria(Indirizzo.class)
							.add(Restrictions.eqOrIsNull("id", varId))
							.uniqueResult();
	}

	@Override
	@Transactional
	public boolean delete(int varId) {
		
		Session session = getSession();
		try {
			Indirizzo temp = session.load(Indirizzo.class, varId);
			
			session.delete(temp);
			session.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	@Override
	@Transactional
	public boolean update(Indirizzo t) {
		
		Session session = getSession();
		try {
			Indirizzo temp = session.load(Indirizzo.class, t.getId());
			if(temp != null) {
				
				temp.setIndirizzo(t.getIndirizzo());
				temp.setCitta(t.getCitta());
				temp.setCap(t.getCap());
				temp.setProvincia(t.getProvincia());
				
				session.update(temp);
				session.flush();
				return true;
			}
		} catch (Exception e) {
				System.out.println(e.getMessage());
		}
		return false;
	}

}
