package com.ultimalezione.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface DataAccessRepo<T> {
	
	T insert(T t);
	
	List<T> findAll();
	
	T findById(int id);
	
	boolean delete(int id);
	
	boolean update(T t);

}
