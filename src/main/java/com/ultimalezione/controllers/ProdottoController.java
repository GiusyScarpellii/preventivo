package com.ultimalezione.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ultimalezione.models.Prodotto;
import com.ultimalezione.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;
	
	
	@PostMapping("/insert")
	public Prodotto inserisciProdotto(@RequestBody Prodotto objPro) {
		return this.service.insert(objPro);
	}
	
	@GetMapping("/cercaTutti")
	public List<Prodotto> preventivi(){
		return this.service.findAll();
	}
	
	@GetMapping("/cercaPerId/{pro_id}")
	public Prodotto cercaPerId(@PathVariable int pro_id) {
		return this.service.findById(pro_id);
	}
	
	@DeleteMapping("/cancella/{pro_id}")
	public boolean eliminaProdotto(@PathVariable int pro_id) {
		return this.service.delete(pro_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaProdotto(@RequestBody Prodotto varPro) {
		return this.service.update(varPro);
	}
	

}
