package com.ultimalezione.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.ultimalezione.models.Cliente;
import com.ultimalezione.models.Indirizzo;
import com.ultimalezione.models.Preventivo;

import com.ultimalezione.services.ClienteService;
import com.ultimalezione.services.IndirizzoService;
import com.ultimalezione.services.PreventivoService;


@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ClienteController {
	

	@Autowired
	private ClienteService service;
	@Autowired
	private PreventivoService serviceP;
	@Autowired
	private IndirizzoService serviceInd;
	
	@PostMapping("/insert")
	public Cliente inserisciCliente(@RequestBody Cliente objCli) {
		return this.service.insert(objCli);
	}
	
	@GetMapping("/cercaTutti")
	public List<Cliente> clienti(){
		return this.service.findAll();
	}
	
	@GetMapping("/cercaPerId/{cli_id}")
	public Cliente cercaPerId(@PathVariable int cli_id) {
		return this.service.findById(cli_id);
	}
	
	@DeleteMapping("/cancella/{cli_id}")
	public boolean eliminaCliente(@PathVariable int cli_id) {
		return this.service.delete(cli_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaCliente(@RequestBody Cliente varCli) {
		return this.service.update(varCli);
	}
	
	
	@GetMapping("/aggiungiIndirizzo/{cli_id}/{ind_id}") 
	public boolean aggiungiIndirizzo(@PathVariable int cli_id, @PathVariable int ind_id) {
		
		Cliente temp = service.findById(cli_id);
		Indirizzo tempp = serviceInd.findById(ind_id);
		temp.setIndirizzo(tempp);
		
		return service.update(temp);
	}
	
	/*
	 * Metodo che mi permette di associare i preventivi ai clienti 
	 */
	@GetMapping("/preventiviClienti/{cliente_id}/{preventivo_id}")
	public boolean preventiviClienti(@PathVariable int cliente_id, @PathVariable int preventivo_id) {
		
		Cliente temp = service.findById(cliente_id);
		List<Cliente> clienti = new ArrayList<Cliente>();
		
		Preventivo tempp = serviceP.findById(preventivo_id);
		List<Preventivo> preventivi = new ArrayList<Preventivo>();
		
		clienti.add(temp);
		preventivi.add(tempp);
		temp.getPreventivi().addAll(preventivi);
		
		return service.update(temp);
	}
	
	

}
