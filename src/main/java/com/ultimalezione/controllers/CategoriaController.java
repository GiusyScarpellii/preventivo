package com.ultimalezione.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ultimalezione.models.Categoria;
import com.ultimalezione.services.CategoriaService;



@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoriaController {
	
	@Autowired
	private CategoriaService service;
	
	
	@PostMapping("/insert")
	public Categoria inserisciCategoria(@RequestBody Categoria objCat) {
		return this.service.insert(objCat);
	}
	
	@GetMapping("/cercaTutti")
	public List<Categoria> categorie(){
		return this.service.findAll();
	}
	
	@GetMapping("/cercaPerId/{cat_id}")
	public Categoria cercaPerId(@PathVariable int cat_id) {
		return this.service.findById(cat_id);
	}
	
	@DeleteMapping("/cancella/{cat_id}")
	public boolean eliminaCategoria(@PathVariable int cat_id) {
		return this.service.delete(cat_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaCategoria(@RequestBody Categoria varCat) {
		return this.service.update(varCat);
	}
	
	

}
