package com.ultimalezione.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ultimalezione.models.Cliente;
import com.ultimalezione.models.Indirizzo;
import com.ultimalezione.models.Preventivo;
import com.ultimalezione.models.Prodotto;
import com.ultimalezione.services.IndirizzoService;
import com.ultimalezione.services.PreventivoService;
import com.ultimalezione.services.ProdottoService;


@RestController
@RequestMapping("/preventivo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PreventivoController {
	
	@Autowired
	private PreventivoService service;
	@Autowired
	private IndirizzoService serviceInd;
	@Autowired
	private ProdottoService serviceProd;
	
	@PostMapping("/insert")
	public Preventivo inserisciPreventivo(@RequestBody Preventivo objPre) {
		return this.service.insert(objPre);
	}
	
	@GetMapping("/cercaTutti")
	public List<Preventivo> preventivi(){
		return this.service.findAll();
	}
	
	@GetMapping("/cercaPerId/{pre_id}")
	public Preventivo cercaPerId(@PathVariable int pre_id) {
		return this.service.findById(pre_id);
	}
	
	@DeleteMapping("/cancella/{pre_id}")
	public boolean eliminaPreventivo(@PathVariable int pre_id) {
		return this.service.delete(pre_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaPreventivo(@RequestBody Preventivo varPre) {
		return this.service.update(varPre);
	}
	
	@GetMapping("/aggiungiIndirizzo/{prev_id}/{ind_id}") 
	public boolean aggiungiIndirizzo(@PathVariable int prev_id, @PathVariable int ind_id) {
		
		Preventivo temp = service.findById(prev_id);
		Indirizzo tempp = serviceInd.findById(ind_id);
		temp.setIndirizzo(tempp);
		
		return service.update(temp);
	}
	
	@GetMapping("/preventiviProdotti/{prev_id}/{prod_id}") 
	public boolean preventiviProdotti(@PathVariable int prev_id, @PathVariable int prod_id) {
		Preventivo temp = service.findById(prev_id);
		List<Preventivo> preventivi = new ArrayList<Preventivo>();
		
		Prodotto tempp = serviceProd.findById(prod_id);
		List<Prodotto> prodotti = new ArrayList<Prodotto>();
		
		preventivi.add(temp);
		prodotti.add(tempp);
		temp.getProdotti().addAll(prodotti);
		
		return service.update(temp);
	}
}
