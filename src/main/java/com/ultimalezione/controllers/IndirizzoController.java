package com.ultimalezione.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ultimalezione.models.Indirizzo;
import com.ultimalezione.services.IndirizzoService;

@RestController
@RequestMapping("/indirizzo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class IndirizzoController {
	

	@Autowired
	private IndirizzoService service;
	
	
	@PostMapping("/insert")
	public Indirizzo inserisciIndirizzo(@RequestBody Indirizzo objInd) {
		return this.service.insert(objInd);
	}
	
	@GetMapping("/cercaTutti")
	public List<Indirizzo> indirizzi(){
		return this.service.findAll();
	}
	
	@GetMapping("/cercaPerId/{ind_id}")
	public Indirizzo cercaPerId(@PathVariable int ind_id) {
		return this.service.findById(ind_id);
	}
	
	@DeleteMapping("/cancella/{ind_id}")
	public boolean eliminaIndirizzo(@PathVariable int ind_id) {
		return this.service.delete(ind_id);
	}
	
	@PutMapping("/modifica/update")
	public boolean modificaIndirizzo(@RequestBody Indirizzo varInd) {
		return this.service.update(varInd);
	}

}
