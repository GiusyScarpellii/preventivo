DROP DATABASE IF EXISTS gestione_preventivi;
CREATE DATABASE gestione_preventivi;
USE gestione_preventivi;

CREATE DATABASE gestione_preventivi;
USE gestione_preventivi;

CREATE TABLE Categoria (
	id_categoria		INT 		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome_categoria		VARCHAR(50) NOT NULL
);

CREATE TABLE Prodotto (
	id_prodotto 	INT 		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    codice_prodotto	VARCHAR(15) NOT NULL UNIQUE,
    nome_prodotto	VARCHAR(50) NOT NULL,
    quantita		INT 		NOT NULL,
    prezzo			FLOAT 		NOT NULL
);

CREATE TABLE Categoria_prodotto (
	categoria	INT 		NOT NULL,
    prodotto	INT 		NOT NULL,
    FOREIGN KEY (categoria) REFERENCES 	Categoria (id_categoria) ON DELETE CASCADE,
    FOREIGN KEY (prodotto)	REFERENCES	Prodotto  (id_prodotto) ON DELETE CASCADE,
    UNIQUE		(categoria, prodotto)
);

CREATE TABLE Indirizzo (
	id_indirizzo	INT 			NOT NULL AUTO_INCREMENT PRIMARY KEY,
    indirizzo		VARCHAR(250)	NOT NULL,
    citta			VARCHAR(50)		NOT NULL,
    cap				VARCHAR(5)		NOT NULL,
    provincia		VARCHAR(2)		NOT NULL
);

CREATE TABLE Preventivo (
	id_preventivo		INT 		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome_azienda		VARCHAR(50)	NOT NULL,
    intestatario		VARCHAR(50)	NOT NULL,
    data_preventivo		DATE	 	NOT NULL,
    codice_preventivo	VARCHAR(15) NOT NULL UNIQUE,
    indirizzo_azienda	INT,
    FOREIGN KEY(indirizzo_azienda) 	REFERENCES Indirizzo (id_indirizzo) ON DELETE CASCADE
);
CREATE TABLE Prodotto_Preventivo (
	prodotto	INT NOT NULL,
    preventivo	INT NOT NULL,
    FOREIGN KEY(prodotto) 	REFERENCES Prodotto   (id_prodotto) ON DELETE CASCADE,
    FOREIGN KEY(preventivo) REFERENCES Preventivo (id_preventivo) ON DELETE CASCADE,
    UNIQUE 	   (prodotto, preventivo)

);

CREATE TABLE Cliente (
	id_cliente		INT 		NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome_azienda	VARCHAR(50)	NOT NULL,
    intestatario	VARCHAR(50)	NOT NULL,
    indirizzo		INT,
    FOREIGN KEY(indirizzo) 	REFERENCES Indirizzo (id_indirizzo) ON DELETE CASCADE
);

CREATE TABLE Cliente_Preventivo (
	cliente			INT		NOT NULL,
    preventivo		INT 	NOT NULL,
	FOREIGN KEY(cliente) 	REFERENCES Cliente 		(id_cliente) ON DELETE CASCADE,
    FOREIGN KEY(preventivo) REFERENCES Preventivo 	(id_preventivo) ON DELETE CASCADE,
    UNIQUE 	   (cliente, preventivo)
);